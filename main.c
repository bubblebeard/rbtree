#include <stdio.h>
#include <stdlib.h>

typedef struct myitem {
    char color;
    int key;
    struct myitem *left;
    struct myitem *right;
    struct myitem *parent;
} node;

node* root = NULL;

void leftRotate(node *);
void rightRotate(node *);
void color_insert(node *);
void insert(int);
void show(node*, int);
int search(int);
node* min(node*);
node* successor(node*);
void color_delete(node*);
node* delete(int);
void del_tree(node*);

int main(int argc, char* argv[]) {

    int choice, value;

    while (1) {
        printf("select action:\n");
        printf("1. insert item\n");
        printf("2. remove item\n");
        printf("3. search\n");
        printf("4. show tree\n");
        printf("5. exit\n");
        scanf("%d", &choice);
        switch (choice) {
            case 1:
                printf("enter integer\n");
                scanf("%d", &value);
                printf("adding new node...\n");
                insert(value);
                break;
            case 2:
                printf("enter integer you want to remove\n");
                scanf("%d", &value);
                delete(value);
                break;
            case 3:
                printf("enter integer you want to find\n");
                scanf("%d", &value);
                search(value);
                break;
            case 4:
                show(root, 0);
                printf("\n");
                break;
            case 5:
                del_tree(root);
                exit(1);
            default:
                printf("unknown command\n");
                break;
        }
    }
    return 0;

}

void leftRotate(node *x) {
    node *y;

    y = x->right;
    x->right = y->left;

    if (y->left != NULL) {
        y->left->parent = x;

    }

    y->parent = x->parent;

    if (x->parent == NULL) {
        root = y;

    } else if ((x->parent->left != NULL) && (x->key == x->parent->left->key)) {
        x->parent->left = y;

    } else x->parent->right = y;

    y->left = x;
    x->parent = y;
    return;

}

void rightRotate(node *y) {
    node *x;

    x = y->left;
    y->left = x->right;

    if (x->right != NULL) {
        x->right->parent = y;

    }

    x->parent = y->parent;

    if (y->parent == NULL) {
        root = x;

    } else if ((y->parent->left != NULL)&& (y->key == y->parent->left->key)) {
        y->parent->left = x;

    } else

        y->parent->right = x;

    x->right = y;
    y->parent = x;

    return;

}

void color_insert(node *z) {
    node *y = NULL;

    while ((z->parent != NULL) && (z->parent->color == 'r')) {

        if ((z->parent->parent->left != NULL) && (z->parent->key == z->parent->parent->left->key)) {
            if (z->parent->parent->right != NULL)

                y = z->parent->parent->right;

            if ((y != NULL) && (y->color == 'r')) {
                z->parent->color = 'b';

                y->color = 'b';

                z->parent->parent->color = 'r';

                if (z->parent->parent != NULL)

                    z = z->parent->parent;

            } else {

                if ((z->parent->right != NULL) && (z->key == z->parent->right->key)) {
                    z = z->parent;

                    leftRotate(z);

                }

                z->parent->color = 'b';

                z->parent->parent->color = 'r';

                rightRotate(z->parent->parent);

            }

        } else {

            if (z->parent->parent->left != NULL)

                y = z->parent->parent->left;

            if ((y != NULL) && (y->color == 'r')) {
                z->parent->color = 'b';

                y->color = 'b';

                z->parent->parent->color = 'r';

                if (z->parent->parent != NULL)

                    z = z->parent->parent;

            } else {

                if ((z->parent->left != NULL) && (z->key == z->parent->left->key)) {
                    z = z->parent;

                    rightRotate(z);

                }

                z->parent->color = 'b';

                z->parent->parent->color = 'r';

                leftRotate(z->parent->parent);

            }

        }

    }
    root->color = 'b';

}

void insert(int val) {
    node *x, *y;

    node *z = (node*) malloc(sizeof (node));

    z->key = val;

    z->left = NULL;

    z->right = NULL;

    z->color = 'r';

    x = root;

    if (search(val) == 1) {
        printf("\nEntered element already exists in the tree\n");

        return;

    }

    if (root == NULL) {
        root = z;

        root->color = 'b';

        return;

    }

    while (x != NULL) {
        y = x;

        if (z->key < x->key) {
            x = x->left;

        } else x = x->right;

    }

    z->parent = y;

    if (y == NULL) {
        root = z;

    } else if (z->key < y->key) {
        y->left = z;

    } else y->right = z;

    color_insert(z);

    return;

}

void show(node *tree, int level) {
    if (tree) {
        show(tree->left, level + 1);
        for (int i = 0; i < level; i++) {
            printf("\t");
        }
        printf("%d\n", tree->key);
        show(tree->right, level + 1);
    }
}

int search(int val) {
    node* temp = root;

    int diff;

    while (temp != NULL) {
        diff = val - temp->key;

        if (diff > 0) {
            temp = temp->right;

        } else if (diff < 0) {
            temp = temp->left;

        } else {
            printf("Search Element Found!!\n");

            return 1;

        }

    }
    printf("Given Data Not Found in RB Tree!!\n");

    return 0;

}

node* min(node *x) {
    while (x->left) {
        x = x->left;

    }
    return x;

}

node* successor(node *x) {
    node *y;

    if (x->right) {
        return min(x->right);

    }
    y = x->parent;

    while (y && x == y->right) {
        x = y;

        y = y->parent;

    }
    return y;

}

void color_delete(node *x) {
    while (x != root && x->color == 'b') {
        node *w = NULL;

        if ((x->parent->left != NULL) && (x == x->parent->left)) {
            w = x->parent->right;

            if ((w != NULL) && (w->color == 'r')) {
                w->color = 'b';

                x->parent->color = 'r';

                leftRotate(x->parent);

                w = x->parent->right;

            }

            if ((w != NULL) && (w->right != NULL) && (w->left != NULL) && (w->left->color == 'b') && (w->right->color == 'b')) {

                w->color = 'r';

                x = x->parent;

            } else if ((w != NULL) && (w->right->color == 'b')) {
                w->left->color = 'b';

                w->color = 'r';

                rightRotate(w);

                w = x->parent->right;

            }

            if (w != NULL) {
                w->color = x->parent->color;

                x->parent->color = 'b';

                w->right->color = 'b';

                leftRotate(x->parent);

                x = root;

            }

        } else if (x->parent != NULL) {
            w = x->parent->left;

            if ((w != NULL) && (w->color == 'r')) {

                w->color = 'b';

                x->parent->color = 'r';

                leftRotate(x->parent);

                if (x->parent != NULL)

                    w = x->parent->left;

            }

            if ((w != NULL) && (w->right != NULL) && (w->left != NULL) && (w->right->color == 'b') && (w->left->color == 'b')) {

                x = x->parent;

            } else if ((w != NULL) && (w->right != NULL) && (w->left != NULL) && (w->left->color == 'b')) {

                w->right->color = 'b';

                w->color = 'r';

                rightRotate(w);

                w = x->parent->left;

            }

            if (x->parent != NULL) {
                w->color = x->parent->color;

                x->parent->color = 'b';

            }

            if (w->left != NULL)

                w->left->color = 'b';

            if (x->parent != NULL)

                leftRotate(x->parent);

            x = root;

        }

    }
    x->color = 'b';

}

node* delete(int var) {
    node *x = NULL, *y = NULL, *z;

    z = root;

    if ((z->left == NULL) &&(z->right == NULL) && (z->key == var)) {
        root = NULL;

        printf("\nRBTREE is empty\n");

        return;

    }

    while (z->key != var && z != NULL) {
        if (var < z->key)

            z = z->left;

        else

            z = z->right;

        if (z == NULL)

            return;

    }

    if ((z->left == NULL) || (z->right == NULL)) {
        y = z;

    } else {
        y = successor(z);

    }

    if (y->left != NULL) {
        x = y->left;

    } else {
        if (y->right != NULL)

            x = y->right;

    }

    if ((x != NULL) && (y->parent != NULL))

        x->parent = y->parent;

    if ((y != NULL) && (x != NULL) && (y->parent == NULL)) {
        root = x;

    } else if (y == y->parent->left) {
        y->parent->left = x;

    } else {
        y->parent->right = x;

    }

    if (y != z) {
        z->key = y->key;

    }

    if ((y != NULL) && (x != NULL) && (y->color == 'b')) {
        color_delete(x);

    }
    return y;

}

void del_tree(node *tree) {
    if (tree) {
        del_tree(tree->left);
        del_tree(tree->right);
        free(tree);
    }
}